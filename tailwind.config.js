const colors = require('tailwindcss/colors');

module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      sans: ['Inter', 'sans-serif'],
    },
    extend: {
      
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    'postcss-import',
    'postcss-nesting',
    'postcss-css-variables'
  ],
}
