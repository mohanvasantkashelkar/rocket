let mix = require('laravel-mix');

  mix.sass('src/scss/tailwind.scss', 'css')
  .setPublicPath('dist')
  .options({
    postCss : [require('tailwindcss')],
    processCssUrls : false
  })
  .browserSync({
    server : './dist',
    files : ['./**/*.html','./dist']
  });